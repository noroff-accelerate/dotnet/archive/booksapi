﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BooksAPI.DTO.Book
{
    public class BookCreateDTO
    {
        public string ISBN10 { get; set; }

        public string ISBN13 { get; set; }

        public string Title { get; set; }

        public string Genre { get; set; }

        public string Edition { get; set; }

        public string Keywords { get; set; }

        public string Publisher { get; set; }

        public DateTime? ReleaseDate { get; set; }

        public string Format { get; set; }

        public string Language { get; set; }

        public int Length { get; set; }

        public string? Description { get; set; }

        public string? CoverImg { get; set; }
    }
}
