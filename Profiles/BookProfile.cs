﻿using AutoMapper;
using BooksAPI.Model;
using BooksAPI.DTO.Book;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BooksAPI.Profiles
{
    public class BookProfile: Profile
    {
        public BookProfile()
        {
            CreateMap<Book, BookReadDTO>()
                            .ForMember(bdto => bdto.Authors, opt => opt
                            .MapFrom(b => b.Authors.Select(b => b.AuthorId).ToArray()));
            CreateMap<BookCreateDTO, Book>();
        }
        
    }
}
