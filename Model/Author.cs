﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BooksAPI.Model
{
    public class Author
    {
        
        public int AuthorId { get; set; }

        [MaxLength(100)]
        public string FullName { get; set; }

        public ICollection<Book> Books { get; set; }
    }
}
