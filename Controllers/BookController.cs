﻿using BooksAPI.Data;
using BooksAPI.Model;
using BooksAPI.DTO.Book;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BooksAPI.DTO.Book;

namespace BooksAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BookController:ControllerBase
    {
        private readonly BookDbContext _dbcontext;
        private readonly IMapper _mapper;

        public BookController(BookDbContext dbContext,IMapper mapper)
        {
            _dbcontext = dbContext;
            _mapper = mapper;
        }

        [HttpGet("All")]
        public async Task<ActionResult<IEnumerable<BookReadDTO>>> GetBooks()
        {
            return _mapper.Map<List<BookReadDTO>>(await _dbcontext.Books
               .Include(b => b.Authors)
               .ToListAsync());
        }

        // GET: api/Books/5
        [HttpGet("{id}")]
        public async Task<ActionResult<BookReadDTO>> GetBook(int id)
        {
            Book book = await _dbcontext.Books.Include(b => b.Authors)
                .FirstOrDefaultAsync(b => b.BookId == id);

            if(book == null)
            {
                return NotFound();
            }

            return _mapper.Map<BookReadDTO>(book);
        }

        [HttpPost]
        public async Task<ActionResult<Book>> PostBook(BookCreateDTO dtoBook)
        {
            Book newBook = _mapper.Map<Book>(dtoBook);
            _dbcontext.Books.Add(newBook);
            await _dbcontext.SaveChangesAsync();

            return CreatedAtAction("GetBook",
                new { id = newBook.BookId },
                _mapper.Map<BookReadDTO>(newBook));
        }
    }
}
